package org.hibernate.LaptopManagement;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class SessionDAO 
{
	
		private static SessionDAO sdao=null;
		private  Configuration conf=null;
		private  SessionFactory sessionf=null;
		private SessionDAO()
		{
			conf=new Configuration();
			conf.configure("config.xml");
			conf.addAnnotatedClass(Laptop.class);
			sessionf = conf.buildSessionFactory();
		}
		public static SessionDAO getInstance()
		{
		if(sdao==null)
		
			sdao=new SessionDAO();
		return sdao;
		}
		
		public void saveDetails(Laptop a)
		{
			Session session=sessionf.openSession();
			Transaction transaction = session.beginTransaction();
			session.save(a);
			transaction.commit();
			System.out.println("Data saved successfully");
		}
		
		public Laptop getDetails(Long id)
		{
			Session session=sessionf.openSession();
			return session.get(Laptop.class, id);
			
		}
		 
		public void updateDetails(Long id,String os)
		{
			Laptop a1=getDetails(id);
			if(a1!=null)
			{
				Session session = sessionf.openSession();
				Transaction transaction = session.beginTransaction();
				a1.setOsType(os);
				session.update(a1);
				transaction.commit();
				System.out.println("details updated");
			}
			else
			{
				System.out.println("Data unavailable,can't update");
			}
			
			
		}
		
		public void deleteDetails(Long id)
		{
			Laptop a1=getDetails(id);
			if(a1!=null)
			{
				Session session=sessionf.openSession();
				Transaction transaction = session.beginTransaction();
				session.delete(a1);
				transaction.commit();
				System.out.println("row is deleted where id="+id);
			}
			else
			{
				System.out.println("id="+id+" does not exist");
			}
		}
		
		
}
