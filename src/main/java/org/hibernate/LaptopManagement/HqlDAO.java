package org.hibernate.LaptopManagement;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;


public class HqlDAO 
{
	private static HqlDAO hsdao=null;
	private  Configuration conf=null;
	private  SessionFactory sessionf=null;
	private HqlDAO()
	{
		conf=new Configuration();
		conf.configure("config.xml");
		conf.addAnnotatedClass(Laptop.class);
		sessionf = conf.buildSessionFactory();
	}
	public static HqlDAO getInstance()
	{
	if(hsdao==null)
	
		hsdao=new HqlDAO();
	return hsdao;
	}
	
	
	public List<Laptop> getLaptopDetails()
	{
		Session session = sessionf.openSession();
		
		String hql="from Laptop";
		
		Query query=session.createQuery(hql);
		
		List<Laptop> list = query.list();
		
		return list;
	}
	
	public Laptop getLaptopDetailsByID(Long id)
	{
		Session session = sessionf.openSession();
		String hql="from Laptop where id=:id";
		Query query=session.createQuery(hql);
		query.setParameter("id", id);
		Laptop res=(Laptop)query.uniqueResult();
		return res;
	}
	
	public void updateOSById(Long id,String os)
	{
		Laptop ldto=getLaptopDetailsByID(id);
		if(ldto!=null)
		{
			Session session = sessionf.openSession();
			Transaction transaction = session.beginTransaction();
			String hql="update Laptop set os_type=:os where id=:id";
			Query query = session.createQuery(hql);
			query.setParameter("os", os);
			query.setParameter("id", id);
			query.executeUpdate();
			transaction.commit();
			System.out.println("update successful");
			
			
		}
		else System.out.println("cant update data not found");
	}
	
	public void deleteDetails(Long id)
	{
		Laptop ldto=getLaptopDetailsByID(id);
		if(ldto!=null)
		{
			Session session = sessionf.openSession();
			Transaction transaction = session.beginTransaction();
			String hql="delete Laptop where id=:id";
			Query query = session.createQuery(hql);
			query.setParameter("id", id);
			query.executeUpdate();
			transaction.commit();
			System.out.println("data deleted where id="+id);
	
		}
		else System.out.println("data not available,cant delete");
	}
}
